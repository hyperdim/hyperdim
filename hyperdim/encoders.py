"""Encoders project data to a high-dimensional space using a list of
high-dimension vectors.
"""
import numpy as np
import multiprocessing
import math

from sklearn.preprocessing import normalize
from .utils import tqdm_controlled as tqdm

import abc

def _wrap_callable(tocall):
    from collections import namedtuple
    Wrapping = namedtuple('Encoder', ['encode'])
    encoder = Wrapping(encode = tocall)
    return encoder

class AbstractEncoder(abc.ABC):
    """Abstract Base Class for encoders.

    Deriving from :class:`abc.ABC`, any class implementing the following methods
    is considered an instance of this class"""
    @abc.abstractmethod
    def encode(self, samples):
        """Encodes *samples* to a fixed length.

        Parameters
        ----------
        samples : array-like of shape `(n_samples, features)`
            Data to encode.

        Returns
        -------
        array-like of shape `(n_samples, d)`
            Encoded samples.
        """

class AbstractBasisEncoder(AbstractEncoder, abc.ABC):
    """Abstract Base Class (:class:`abc.ABC`) for encoders that work using a
    vector basis.

    Not to be used directly, will raise ``NotImplementedError``.

    Parameters
    ----------
    d : int, >= 1
        Number of dimensions for the encoder. This corresponds to the number
        of dimensions of the hypervectors.

    features : int, >= 1
        Number of hypervectors for the encoders basis. This corresponds to
        the number of features for the (unencoded) input data.

    Attributes
    ----------
    d : int, >= 1
        Number of dimensions for the encoder. This corresponds to the number
        of dimensions of the hypervectors.

    features : int, >= 1
        Number of hypervectors for the encoders basis. This corresponds to
        the number of features for the (unencoded) input data.

    basis : array-like of shape `(d, features)``
        Array of high-dimensional vectors.

    """
    def __init__(self, d, features):
        if not (d >= 1 and int(d) == d):
            raise ValueError("Invalid number of dimensions {}".format(d))
        self.d = d
        self.features = features
        self.basis = self._create_basis()

    @abc.abstractmethod
    def _create_basis(self):
        raise NotImplementedError

    @abc.abstractmethod
    def encode_sample(self, sample):
        """Encodes the sample using the generated vectors.

        Parameters
        ----------
        sample : array-like of shape `(n_samples, features)`
            Sample to encode.

        Returns
        -------
        array-like of shape `(d,)`
            Encoded sample.

        """
        pass

    def encode(self, samples, use_multiprocessing = True, verbose = 0):
        """Encodes the samples using the generated vectors.

        Parameters
        ----------
        samples : array-like of shape `(n_samples, features)`
            Data to encode.

        use_multiprocessing : bool
            If False multiprocessing will not be used.

        verbose : int, {0, 1}
            If 0 no messages will be printed.

        Returns
        -------
        array-like of shape `(n_samples, d)`
            Encoded samples.
        """
        samples = np.array(samples).reshape(len(samples), self.features)

        samples = normalize(samples, norm='l2')
        if not use_multiprocessing:
            basis = list()
            it = tqdm(samples, verbose=verbose)
            for sample in it:
                basis.append(self.encode_sample(sample))
            return np.asarray(basis)

        with multiprocessing.Pool() as pool:
            it = pool.imap(self.encode_sample, samples, chunksize=100)
            it = tqdm(it, verbose=verbose, total=len(samples), desc="Encoding")
            res = np.array(list(it))
        return res


class EncoderNonLinear(AbstractBasisEncoder):
    """Non linear encoder.

    Parameters
    ----------
    d : int, >= 1
        Number of dimensions for the encoder. This corresponds to the number
        of dimensions of the hypervectors.

    features : int, >= 1
        Number of hypervectors for the encoders basis. This corresponds to
        the number of features for the (unencoded) input data.

    mu : float
        Mean of the normal distribution that will be used to generate the
        vectors.

    sigma : float
        Standard deviation of the normal distribution that will be used to
        generate the vectors.

    Attributes
    ----------
    d : int, >= 1
        Number of dimensions for the encoder. This corresponds to the number
        of dimensions of the hypervectors.

    features : int, >= 1
        Number of hypervectors for the encoders basis. This corresponds to
        the number of features for the (unencoded) input data.

    basis : array-like of shape `(d, features)`
        Array of high-dimensional vectors.

    base : array-like of shape `(d,)`
        Basis offset.
    """
    def __init__(self,d,features,mu = 0.0, sigma=1.0):
        self.mu = mu
        self.sigma = 1.0
        self.base = np.random.uniform(0, 2*math.pi, d)
        super().__init__(d,features)

    def _create_basis(self):
        basis = list()
        it = range(self.d)
        for i in it:
            basis.append(np.random.normal(self.mu, self.sigma, self.features))
        return np.asarray(basis)

    def encode_sample(self, sample):
        encoded = np.empty(self.d)
        for i in range(self.d):
            encoded[i] = np.cos(np.dot(sample, self.basis[i]) + self.base[i])
            encoded[i] *= np.sin(np.dot(sample, self.basis[i]))
        return encoded

class EncoderLinear(AbstractBasisEncoder):
    """Linear Encoder class.

    Parameters
    ----------
    d : int, >= 1
        Dimensions of the hypervectors.

    features : int, >= 1
        Number of hypervectors for the basis. Corresponds to the number of
        features for the (unencoded) input data.

    Attributes
    ----------
    d : int, >= 1
        Dimensions of the hypervectors.

    features : int, >= 1
        Number of hypervectors for the encoders basis. This corresponds to
        the number of features for the (unencoded) input data.

    basis : array-like of shape `(d, features)`
        Array of high-dimensional vectors.

    """
    def _create_basis(self):
        base = np.zeros(self.d)
        basis = list()

        for i in range(int(self.d/2)):
            base[i] = 1
        for i in range(int(self.d/2), self.d):
            base[i] = -1

        it = range(self.features)
        for i in it:
            basis.append(np.random.permutation(base))
        return np.asarray(basis)

    def encode_sample(self, sample):
        encoded = np.zeros(self.d)
        for i in range(self.features):
            encoded += sample[i]*self.basis[i]
        return encoded

ENCODERS = {
    'linear':EncoderLinear,
    'nonlinear':EncoderNonLinear,
}

def _get_class(encoder):
    try:
        return ENCODERS[encoder]
    except:
        raise ValueError('Could not interpret encoder identifier: ' + str(encoder))
