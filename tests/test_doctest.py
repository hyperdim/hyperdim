"""Test the docstring examples to ensure they run as written"""

import unittest
import doctest
import hyperdim.hdmodel
import hyperdim.encoders
import hyperdim.kernels
import hyperdim.utils
import hyperdim.serialization

modules = [
    hyperdim.hdmodel,
    hyperdim.encoders,
    hyperdim.kernels,
    hyperdim.utils,
    hyperdim.serialization,
]

def load_tests(loader, tests, ignore):
    for module in modules:
        tests.addTests(doctest.DocTestSuite(module))
    return tests
