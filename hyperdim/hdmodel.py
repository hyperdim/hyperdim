"""An HDModel (short for HyperDimensional Model) is a class of machine learning
models based on high dimension random vectors.

https://ieeexplore.ieee.org/abstract/document/8012434/

"""
from pathlib import Path

from collections import defaultdict, namedtuple
from functools import reduce
from statistics import mean

import numpy as np

from . import encoders
from . import kernels

class HDModel:
    """The basic Hyperdimensional Model. Encodes input data to a
    hyperdimensional space and stores one hypervector for each class.

    Parameters
    ----------
    features : int, >= 1 or tuple
        Number of features, or shape of the unencoded input data.

    classes : int, >= 1
        Number of classes for the target data.

    encoder : ~collections.abc.Callable or AbstractEncoder or str
        Specifies the encoder for the model.

        If :class:`str`, must be one of 'linear' or 'nonlinear'. The corresponding
        encoder class will be instanced with default optional parameters.

        If :class:`~collections.abc.Callable`, its behaviour must match
        :func:`~hyperdim.encoders.AbstractEncoder.encode`.

    encoder_args : dict
        Parameter dictionary that will be passed when calling the encoding function.

    d : int, >= 1
        Number of dimensions of the class hypervectors.

    learning_rate : float, >= 0
        Learning rate.

    kernel : ~collections.abc.Callable or str
        Specifies the kernel function for the model.

        If str, must be one of {‘dot’, ‘gauss’, ‘poly’, 'cos’}.

    kernel_args : dict
        Parameter dictionary that will be passed when calling the kernel function.

    Attributes
    ----------
    features : int, >= 1 or tuple
        Number of features, or shape of the unencoded input data.

    classes : int, >= 1
        Number of classes for the target data.

    d : int, >= 1
        Number of dimensions of the class hypervectors.

    weights : array-like of shape `(classes, d)`
        Class hypervectors.

    See Also
    --------
    hyperdim.kernels : Available kernel functions.
    hyperdim.encoders : Predefined encoders.

    """

    def __init__(self,
            features,
            classes,
            encoder='nonlinear',
            encoder_args = dict(),
            d=20000,
            learning_rate=0.035,
            kernel='cos',
            kernel_args = dict()):
        # Tuplize integers
        if isinstance(features,int): features = [features]

        if not learning_rate >= 0:
            raise ValueError("Invalidad training rate {}".format(learning_rate))
        if not (d >= 1 and int(d) == d):
            raise ValueError("Invalid number of dimensions {}".format(d))
        for f in features:
            if not isinstance(f, int) or f < 1:
                raise ValueError("Non-integer or < 1 features {}".format(features))
        if not (classes >= 1 and int(classes) == classes):
            raise ValueError("Invalid number of features {}".format(classes))
        # Initialize core attributes
        self.features = features
        self.classes = classes
        self.d = d
        self.weights = np.zeros((self.classes, self.d))

        self.learning_rate = learning_rate

        # encoder can be: str ID, callable, or have the encoder.encode(samples)
        # attribute
        if isinstance(encoder, str):
            n_features = reduce(lambda x,y: x*y, self.features)
            encoder = encoders._get_class(encoder)(d, n_features)
        elif callable(encoder):
            encoder = encoders._wrap_callable(encoder)
        elif not callable(getattr(encoder, "encode", None)):
            raise ValueError("Given encoder is not callable and has no encoder()\
                    callable attribute")

        self.encoder = encoder
        self.encoder_args = encoder_args


        # kernel cam be: str ID or callable
        if isinstance(kernel, str):
            kernel = kernels._get_kernel(kernel)
        elif not callable(kernel):
            raise ValueError("Given kernel is not callable")

        self.kernel = kernel
        self.kernel_args = kernel_args

    def fit(self,
            x,
            y,
            epochs=1,
            initial_epoch=0,
            verbose=0,
            validation_split=0.,
            validation_data=None,
            sample_weight=None,
            class_weight=None,
            encode=True,
            **kwargs):
        """Train the model on the given data.

        Parameters
        ----------
        encode : bool
            If `False` *x* will not be encoded.

        x : array-like
            Data to train on. Must have shape
            `(n_samples, features)` if ``encode = True`` or
            `(n_samples, d)` if ``encode = False``.

        y : array-like with shape `(n_samples, classes)`.
            Target categorical labels.

        epochs : int, >= 1
            Number of epochs (including one shot training).

        verbose : int, {0, 1}
            If 0 no messages will be printed.

        validation_split :  float, between 0 and 1
            Fraction of the training data to be used as validation data. The
            model will set apart this fraction of the training data, will
            not train on it, and will evaluate the loss and any model
            metrics on this data at the end of each epoch. The validation
            data is selected from the last samples in the x and y data
            provided, before shuffling.

        validation_data : tuple
            Data on which to evaluate model metrics at the end of
            each epoch. The model will not be trained on this data.
            `validation_data` will override validation_split.

            Must be tuple `(x_val, y_val)` with the same restrictions as *x* and
            *y* above.

        class_weight : dict[int,float]
            A dictionary mapping class indices  to a weight value, used for
            weighting the samples during training only. This can be useful to
            tell the model to "pay more attention" to samples from an
            under-represented class.

        sample_weight : array-like of shape `(n_samples)`
            Optional array of weights for the training samples, used for
            weighting during training only (1:1 mapping between weights
            and samples).

        Returns
        -------
        dict[str,list]
            Dictionary with keys ``["acc"]`` (and ``"val_acc"`` if validation_split > 0)
            mapping metrics to an array-like of size *epochs* with the
            corresponding values for each epoch.

        """
        # Test x and y
        if len(x) != len(y):
            raise ValueError("len(x) = {} and len(y) = {} do not\
                    match".format(len(x), len(y)))

        # Setup and test x_val and y_val
        if validation_data is not None:
            # Data is already splitted, test size
            if len(validation_data) != 2:
                raise ValueError('When passing validation_data, '
                                 'it must contain 2 (x_val, y_val) '
                                 'items, however it contains {}'
                                 'items'.format(len(validation_data)))
            x_val, y_val = validation_data
            if len(x_val) != len(y_val):
                raise ValueError("len(x_val) = {} and len(y_val) = {} do not\
                        match".format(len(x_val), len(y_val)))
        else:
            # Split data, ensuring size
            if not validation_split <= 1 and validation_split >= 0:
                raise ValueError("Invalid validation_split {}".format(validation_split))
            data = [(x[i],y[i]) for i in range(len(x))]
            data_val = data[:int(len(data)*validation_split)]
            data = data[int(len(data)*validation_split):]
            def xyize(xys): return ([d[0] for d in xys], [d[1] for d in xys])
            x, y = xyize(data)
            x_val, y_val = xyize(data_val)

        if len(x) > 0:
            x, y = self._standarize_data(x,y, encode)
        if len(x_val) > 0:
            x_val, y_val = self._standarize_data(x_val, y_val, encode)

        # Setup default sample and class weights
        if sample_weight is None: sample_weight = [1 for _ in range(len(x))]
        if class_weight is None: class_weight = {i : 1 for i in range(self.classes)}

        # Perform argument checks
        if len(sample_weight) != len(x):
            raise ValueError("Invalid sample_weight with len {}".format(len(sample_weight)))

        if set(class_weight.keys()) != set([i for i in range(self.classes)]):
            raise ValueError("Invalid class_weight keys")

        if not isinstance(epochs,int) and epochs >= 1:
            raise ValueError("Invalid number of epochs: {}".format(epochs))

        if not verbose in [0,1]:
            raise ValueError("Verbose level {} invalid".format(verbose))

        if verbose:
            if len(x_val) > 0:
                print("Training on {} samples validate on {}".format(len(x),len(x_val)))
            else:
                print("Training on {} samples".format(len(x)))

        history = {"acc" : []}
        if len(x_val) > 0:
            history["val_acc"] = []

        for j in range(initial_epoch, initial_epoch + epochs):
            # One shot fit
            if verbose: print("Epoch {}/{}".format(j+1, epochs))
            if j == 0:
                for i,(xi, yi) in enumerate(zip(x,y)):
                    label = np.argmax(yi)
                    xi_weight = self.learning_rate*class_weight[label]*sample_weight[i]
                    self.weights[label] += xi_weight*xi

            else:
                # Epoch train
                for i, (xi, yi) in enumerate(zip(x,y)):
                    guess = np.argmax(self.predict([xi], encode = False)[0])
                    label = np.argmax(yi)

                    # If the guess was wrong, update weights
                    if guess != label:
                        xi_weight_g = self.learning_rate*class_weight[guess]*sample_weight[i]
                        xi_weight_l = self.learning_rate*class_weight[label]*sample_weight[i]
                        self.weights[guess] -= xi_weight_g*xi
                        self.weights[label] += xi_weight_l*xi

            # Calculate metrics
            acc = self.evaluate(x, y, encode=False, verbose = verbose)
            history["acc"].append(acc)

            if len(x_val) > 0:
                val_acc = self.evaluate(x_val, y_val, encode=False, verbose = verbose)
                history["val_acc"].append(val_acc)
                if verbose: print("acc: {}, val_acc: {}".format(acc, val_acc))


        #History = namedtuple('History', ['history'])
        #h = History(history)

        # Return raw dict
        return history

    def fit_generator(self,
            generator,
            verbose=0,
            encode=False,
            **kwargs):
        """Call :func:`~hyperdim.hdmodel.HDModel.fit`
        for each `(x,y)` encoded data yielded by *generator* with *kwargs*.

        Parameters
        ----------
        generator : ~collections.abc.Generator
            Data to train on. Must yield ``(x,y)`` with the same restrictions as
            :func:`~hyperdim.hdmodel.HDModel.fit`.

        verbose : int, {0, 1}
            If 0 no messages will be printed.

        encode : bool
            If `False` *x* will not be encoded.

        kwargs : dict
            Arguments to :func:`~hyperdim.hdmodel.HDModel.fit`.


        Returns
        -------
        dict[str,list]
            Dictionary with keys ["acc"] mapping metrics to an array of size 1
            with the epoch accuracy.

        See Also
        --------
        hyperdim.serialization : utilities to create generators.

        Example
        -------
        To train for various epochs consider the following example using a mock
        generator:

        >>> from hyperdim.hdmodel import HDModel
        >>> from hyperdim.utils import random_dataset
        >>> d = 100
        >>> features, classes = d,2
        >>> epochs = 3
        >>> model = HDModel(features, classes, d=d)
        >>> x,y = random_dataset(samples=10,features=features,classes=classes)
        >>> for epoch in range(epochs):
        ...   gen = zip(x,y)
        ...   h = model.fit_generator(gen, initial_epoch=epoch, verbose=0)

        See Also
        --------
        hyperdim.serialization : includes tools to create generators from files

        """
        if verbose: print("Training on generator.")
        history = defaultdict(list)
        for x,y in generator:
            hist = self.fit([x],[y],
                    encode=encode,
                    verbose=verbose,
                    **kwargs)
            for k in hist.keys(): history[k] = hist[k]
        return history

    def predict(self, x, encode=True, verbose = 0):
        """Predict on the data.

        Parameters
        ----------
        encode : bool
            If `False` *x* will not be encoded.

        x : array-like
            Data to predict on. Of shape
            `(n_samples, features)` if ``encode = True`` or
            `(n_samples, d)` if ``encode = False``.

        verbose : {0, 1}, int
            If 0 no messages will be printed.

        Returns
        -------
        array-like of shape `(n_samples, classes)`
            Distances of each sample to the hypervector of each class.
        """
        ## LOW MEMORY TEST
        predictions = list()
        for xi in x:
            xi = self._standarize_data([xi],None,encode)
            xi = xi[0]
            predictions.append([self.kernel(self.weights[m], xi,
                **self.kernel_args) for m in range(self.classes)])
        return np.array(predictions)

    def evaluate(self, x, y, encode=True, verbose = 0):
        """Evaluate the model using the given (x[i],y[i]) pairs.

        Parameters
        ----------
        x : array-like
            Data to predict on. Must have shape compatible with `(n_samples,
            features)` if ``encode = True`` or `(n_samples,
            d)` if ``encode = False``
        y : array-like of shape `(n_samples, classes)`
            Target categorical labels.
        encode : bool
            If `False` *x* will not be encoded.
        verbose : {0,1}, int
            If 0 no messages will be printed.

        Returns
        -------
        acc : float
            fraction on the input on which the models correctly predicted the
            target data.

        """
        ## LOW MEMORY VERSION TEST
        acc = list()
        for xi, yi in zip(x,y):
            xi, yi = self._standarize_data([xi],[yi], encode)
            yip = self.predict(xi, encode=False, verbose=verbose)
            yi = np.argmax(yi, axis=-1)
            yip = np.argmax(yip, axis=-1)
            acc.append(int(yi[0] == yip[0]))
        return mean(acc)

    def _standarize_data(self, x, y, encode : bool):
        """
        For internal use. Returns the encoded-x[] and y[] inputs.

        Converts x and y to numpy arrays and encodes x (if encode is True).
        Performs checks on the data that may raise ValueError.

        Parameters
        ----------
        encode : bool
            If `False` *x* will not be encoded.

        x : array-like or None
            Data to predict on. Must have shape compatible with `(n_samples,
            features)` if ``encode = True`` or `(n_samples,
            d)` if ``encode = False``

        y : array-like of shape `(n_samples, classes)` or None
            Target categorical labels.

        Returns
        -------
        tuple of array-like, or array-like
            Standarized :class:`numpy.array`

        """
        if y is not None:
            y = np.array(y)
            if y.shape != (len(x), self.classes):
                raise ValueError("Invalid target data shape with y shape {} should be {}".format(y.shape, (len(x), self.classes)))

        if x is not None:
            x = np.array(x)

            if encode:
                # Data must be compatible with encoder
                # Leave the check to the encoder!
                #unencoded_shape = (len(x), *self.features)
                #if x.shape != unencoded_shape:
                #    raise ValueError("Input x shape {} incompatible with expected shape {}".format(x.shape, unencoded_shape))
                x_enc = self.encoder.encode(x, **self.encoder_args)
            else:
                x_enc = x

            # Encoded data must be compatible with HD dims
            encoded_shape = (len(x), self.d)

            if x_enc.shape != encoded_shape:
                raise ValueError("Encoded x shape {} incompatible with shape\
                    {}".format(x_enc.shape, encoded_shape))

        if y is not None and x is not None: return x_enc, y
        elif x is not None: return x_enc
        else: return y

    def summary(self):
        """Print a short summary of the model to standard output"""
        print("HDModel with dimension {} and classes {}. Input dimensions {}".format(self.d, self.classes, self.features))
