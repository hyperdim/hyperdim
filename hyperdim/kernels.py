"""Kernel functions for hypervectors.
"""

import numpy as np

def gauss_kernel(x, y, std : float=25):
    """
    Parameters
    ----------
    x : array-like with shape `(d,)`
        Hypervector for the kernel.
    y : array-like with shape `(d,)`
        Hypervector for the kernel.
    std : float
        Kernel parameter.
    """
    res = np.linalg.norm(x - y)
    res = res**2
    res *= -1
    res = n/(2*(std**2))
    res = np.exp(n)
    return res

def poly_kernel(x, y, c=3, d=5):
    """
    Parameters
    ----------
    x : array-like with shape `(d,)`
        Hypervector for the kernel.
    y : array-like with shape `(d,)`
        Hypervector for the kernel.
    c : int
        Kernel parameter.
    d : int
        Kernel parameter.
    """
    return (np.dot(x, y) + c)**d

def cos_kernel(x, y):
    """
    Parameters
    ----------
    x : array-like with shape `(d,)`
        Hypervector for the kernel.
    y : array-like with shape `(d,)`
        Hypervector for the kernel.
    """
    if np.linalg.norm(x)==0 or np.linalg.norm(y) == 0: return 0
    return np.dot(x, y)/(np.linalg.norm(x)*np.linalg.norm(y))

KERNELS = {
        'dot': np.dot,
        'gauss': gauss_kernel,
        'poly': poly_kernel,
        'cos': cos_kernel
}

def _get_kernel(kernel):
    """
    Parameters
    ---------
    kernel: string
        Specifies the kernel function for the model. Must be one of {‘dot’,
        ‘gauss’, ‘poly’, 'cos’}.

    Returns
    -------
    kernel: function
        The specified kernel function.
    """
    return KERNELS[kernel]
