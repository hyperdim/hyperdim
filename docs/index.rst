.. Sphinx Example Project documentation master file, created by
   sphinx-quickstart on Tue Oct 25 09:18:20 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to hyperdim
==================================================

An HDModel (short for HyperDimensional Model) is a class of machine learning
models based on high dimension random vectors.

Contents:

.. toctree::
   :maxdepth: 1

   source code on gitlab <https://gitlab.com/hyperdim/hyperdim/>
   examples

API:

.. autosummary::
   :toctree: _autosummary
   :nosignatures:

   hyperdim.hdmodel
   hyperdim.encoders
   hyperdim.kernels
   hyperdim.utils
   hyperdim.serialization


Quickstart
----------

If you are using keras or sklearn it will be easy to use hyperdim.
Fork this repository and put it in you projects folder or use pip or pipenv:

``$ pip install --user hyperdim``

.. literalinclude:: ../examples/hello_world.py

Source code
-----------

* https://gitlab.com/hyperdim/hyperdim
