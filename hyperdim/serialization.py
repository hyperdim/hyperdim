"""Serialization for (huge) datasets and models.

"""
import numpy as np
import multiprocessing
import math
from .utils import tqdm_controlled as tqdm
from collections.abc import Sized, Iterable

int_size = 4

def _int_to_bytes(x : int) -> bytes:
    return x.to_bytes(int_size, 'little')

def _bytes_to_int(byts : bytes):
    return int.from_bytes(byts, 'little')

class EncodedFileIterator(Sized, Iterable):
    """Iterable representation of a file created by :func:`encode_to_file`.

    This class is a :class:`~collections.abc.Sized`
    :class:`~collections.abc.Iterable`, which means you can use instances it as
    iterators and access their length via :func:`len`.

    Parameters
    ----------
    filename : str
        Must be a pathlike object, which will be passed to :func:`open`.

    x_dtype : str
        Numpy data type for the `x` data.

    y_dtype : str
        Numpy data type for the `y` data.

    Attributes
    ----------
    filename : str
        Must be a pathlike object, which will be passed to :func:`open`.

    x_dtype : str
        Numpy data type for the `x` data.

    y_dtype : str
        Numpy data type for the `y` data.

    x_size : int
        Length in bytes of the `x` data.

    y_size : int
        Length in bytes of the `y` data.

    length : int
        Length of the iterator.

    See also
    --------
    encode_to_file : the function that creates the file
    generate_from_file : the function that reads the file created

    """
    def __init__(self, filename, x_dtype = 'float64', y_dtype = 'float64'):
        with open(filename, "rb") as f:
            length = _bytes_to_int(f.read(int_size))
            x_size = _bytes_to_int(f.read(int_size))
            y_size = _bytes_to_int(f.read(int_size))
        self.filename = filename
        self.x_dtype  = x_dtype
        self.y_dtype  = y_dtype
        self.x_size   = x_size
        self.y_size   = y_size
        self.length   = length

    def __len__(self):
        return self.length

    def generate(self):
        chunk_size = self.x_size + self.y_size
        with open(self.filename, "rb") as f:
            n_xys  = _bytes_to_int(f.read(int_size))
            x_size = _bytes_to_int(f.read(int_size))
            y_size = _bytes_to_int(f.read(int_size))
            while True:
                x_data = f.read(x_size)
                if len(x_data) == 0: break
                y_data = f.read(y_size)

                x = np.frombuffer(x_data, dtype = self.x_dtype)
                y = np.frombuffer(y_data, dtype = self.y_dtype)
                yield x,y

    def __iter__(self):
        """Iterate through the `(x,y)` pairs stored in
        `filename` without storing all data in memory.

        Yields
        -------
        tuple
            `(x,y)` pairs with ``x.shape == (features,)`` and ``y.shape ==
            (classes,)`` as saved in the file.
        """
        return self.generate()

def generate_from_file(filename, x_dtype = 'float64', y_dtype = 'float64'):
    """Iterate through the `(x,y)` pairs stored in
    `filename` without storing all data in memory.

    This is specially useful for large datasets that have been encoded and saved
    using the encode_to_file function.

    Parameters
    ----------
    filename : str
        File to iterate through.
    x_dtype : str
        Numpy dtype used to store the x data.
    y_dtype : str
        Numpy dtype used to store the y data.

    Returns
    -------
    EncodedFileIterator
        Iterable object that will yield `(x,y)` pairs with ``x.shape ==
        (features,)`` and ``y.shape == (classes,)`` as saved in the file.

    See also
    --------
    encode_to_file : the function that creates the file
    """
    return EncodedFileIterator(filename,x_dtype=x_dtype,y_dtype=y_dtype)

def encode_to_file(load, load_params, encoder, filename, x_dtype = 'float64',
        y_dtype = 'float64',verbose = 0):
    """Encode and save data to file without simultaneously storing everything in
    memory.

    Effectively this becomes: for each `(x,y)` pair returned by
    ``map(load,load_params)`` encode ``x_e = encoder.encode(x)`` and save to
    *filename* `(x_e, x)`.

    This is used for large datasets that will not fit into memory.

    Parameters
    ----------
    load : ~collections.abc.Callable
        :class:`~collections.abc.Callable` that returns `(x,y)` pairs with ``x.shape == (features,)`` and
        ``y.shape == (classes,)``.

    load_params : iterable
        parameters for *load*.

    encoder : AbstractEncoder
        Encoder instance.

    filename : str
        File to iterate through.

    x_dtype : str
        Numpy dtype that will be used to store the x data.

    y_dtype : str
        Numpy dtype that will be used to store the y data.

    verbose : int, {0, 1}
        If 0 no messages will be printed.


    See also
    --------
    generate_from_file : the function that reads the file created

    Example
    -------
    Consider a function `img_open(filename)` that returns an (x,y) pair.

    For demonstration purposes we simulate reading from file.

    >>> from numpy.random import random_sample as rd
    >>> from hyperdim.utils import to_categorical as tc
    >>> from hyperdim.encoders import EncoderNonLinear
    >>> # mock img_open
    >>> def img_open(filename):
    ...     return (rd(5), tc(0,num_classes=2))
    >>> filenames      = ["0/img1.png","0/img2.png", "0/img3.png"]
    >>> x, y           = img_open(filenames[0])
    >>> print(x.shape) # vectorization of the image (features,)
    (5,)
    >>> print(y.shape) # categorical class (classes,)
    (2,)
    >>> enc            = EncoderNonLinear(d=1000, features=5)
    >>> # Save encoded x,y pairs
    >>> encode_to_file(img_open, filenames, enc, "dataset.hdim")
    >>> generator      = generate_from_file("dataset.hdim")
    >>> # Retrieve x,y pairs from file one by one
    >>> for x, y in generator:
    ...    print(x.shape)
    ...    print(y.shape)
    ...    break
    (1000,)
    (2,)
    >>> # Delete generated file
    >>> from os import remove
    >>> remove("dataset.hdim")
    """
    with open(filename, 'wb') as f:
        first_param_flag = True
        load_params = tqdm(load_params, verbose=verbose, desc="Encoding to file")
        for param in load_params:
            sample, label = load(param)
            sample = encoder.encode([sample])[0]

            sample = np.array(sample, dtype = x_dtype)
            label = np.array(label, dtype = y_dtype)

            if first_param_flag:
                f.write(_int_to_bytes(len(load_params)))
                f.write(_int_to_bytes(len(sample.tobytes())))
                f.write(_int_to_bytes(len(label.tobytes())))
                first_param_flag = False

            f.write(sample.tobytes())
            f.write(label.tobytes())
