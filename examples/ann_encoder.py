"""Use the output of an intermediate layer as the encoder for an HDModel"""

from hyperdim.hdmodel import HDModel
from hyperdim.utils import random_dataset
from functools import reduce
import numpy as np
try:
    from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten
    from tensorflow.keras.layers import Conv2D, MaxPooling2D
    from tensorflow.keras.layers import Conv2D, MaxPooling2D
    from tensorflow.keras.models import Sequential
    from tensorflow.keras.models import Model
except:
    print("You need tensorflow to run this example")
    exit()

class ANNEncoder:
    """Use the output of a layer from a Keras model as encoder.

    The encoding process for a sample is equivalent to using the
    flattened ann output for that sample.

    If the ann has output shape `(None,x1,x2,...)`, then `self.d` will be
    `x1*x2*x3*...` (will flatten).

    It is assumed that:
     - ann has output (None,classes)
     - ann gives ann.get_layer(name=layer_name)

    Parameters
    ----------
    ann : Keras model
        Model whose output will be used to encode samples.

    layer_name : str
        Name of the keras model layer

    """
    def __init__(self, ann, layer_name):
        ann_cut     = Model(inputs=ann.input, outputs=ann.get_layer(name=layer_name).output)
        dims        = ann_cut.get_layer(index=-1).output_shape[1:]
        self.d      = reduce(lambda x,y: x*y, dims)
        # Build intermediate ann as encoder
        self.ann   = ann_cut

    def encode(self, samples, verbose = 0, **kwargs):
        """Encodes the samples using the ann."""
        predictions = self.ann.predict(samples)
        encoded_samples = list()
        for prediction in predictions:
            encoded_samples.append(prediction.flatten())
        return np.array(encoded_samples)

# Build mock dataset of 64x64 RGB images
x, y = random_dataset(samples=100, features=(64,64,3), classes=5)

# Split dataset
validation_split = 0.4
data = [(x[i],y[i]) for i in range(len(x))]
data_val = data[:int(len(data)*validation_split)]
data = data[int(len(data)*validation_split):]
def xyize(xys): return map(np.array,([d[0] for d in xys], [d[1] for d in xys]))
x, y = xyize(data)
x_val, y_val = xyize(data_val)

# Build the ANN
ann = Sequential()
ann.add(Conv2D(32, (3, 3), padding='same',
               input_shape=x.shape[1:]))

ann.add(Activation('relu'))
ann.add(Conv2D(32, (3, 3)))
ann.add(Activation('relu'))
# Mark this layer as target
ann.add(MaxPooling2D(pool_size=(2, 2), name="target"))

# Target layer shape: (None,31,31,32)
print("Target layer shape: {}".format(ann.get_layer(index=-1).output_shape))

ann.add(Dropout(0.25))
ann.add(Flatten())
ann.add(Dense(512))
ann.add(Activation('relu'))
ann.add(Dropout(0.5))
ann.add(Dense(y.shape[1]))
ann.add(Activation('softmax'))
ann.compile(loss='categorical_crossentropy',
        metrics=['accuracy'],
        optimizer='sgd')

# Train the ANN
ann_hist = ann.fit(x, y,
        validation_data=(x_val, y_val),
        epochs = 3,
        batch_size = 32,
        verbose = 1)
print("ANN hist: {}".format(ann_hist.history))

# Build Encoder using the target layer
encoder = ANNEncoder(ann, "target")
hd_y_shape = ann.get_layer(index=-1).output_shape[1]
hd_x_shape = ann.get_layer(index=0).input_shape[1:]

# Build HD Model with the ANNEncoder
hd = HDModel(hd_x_shape, hd_y_shape, d=encoder.d, encoder=encoder)
print("HDModel dimensions: {}".format(hd.d)) # d = 31*31*32

# Train HD Model
hd_hist = hd.fit(x, y, validation_data=(x_val, y_val), verbose = 1)
print("HDModel hist: {}".format(hd_hist))
