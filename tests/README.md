If you are contributing or otherwise modifying hyperdim you may want to run the
unitary tests.

1. Install the package in developer mode
```
$ git clone ...
$ cd hyperdim
$ pip install -e . --user
```

2. Modify the source code

3. Run the tests
```
$ cd tests
$ python -m unittest -c
$
```

4. If only a short message reporting the number of tests is printed, every test
	 passed.
