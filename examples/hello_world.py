from hyperdim.hdmodel import HDModel
from hyperdim.utils import to_categorical
import numpy as np

# Random dataset of (x,y) pairs with 15 features and 5 classes.
samples = 100
features = 15
classes = 5
x = np.random.random(size = (samples, features))
y = [ int(np.random.random()*classes) for _ in x ]

# Convert the y array to categorical representation.
y = to_categorical(y)
print(x.shape) # (samples, features)
print(y.shape) # (samples, classes)

# Build a model
dimensions = 10000
model = HDModel(features, classes, d = dimensions)

# Fit using 30% of the data for validation, using one_shot_fit only
history = model.fit(x, y, validation_split = 0.3, epochs = 1)

print(history["acc"])
print(history["val_acc"])
