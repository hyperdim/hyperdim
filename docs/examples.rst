examples
========

.. jinja:: examples_context
  :header_char: -

  {% for title, file in examples %}
  {{title}}
  {{ options.header_char * 50 }}
  .. literalinclude:: ../examples/{{file}}
  {% endfor %}
