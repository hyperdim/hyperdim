"""Provides ease-of-use functions for working with datasets.

"""

import numpy as np
from tqdm import tqdm

def to_categorical(y, num_classes=None, dtype='float32'):
    """Converts a class vector (integers) to binary class matrix.
    E.g. for use with hyperdim.hdmodel.HDModel

    Parameters
    ----------
    y : :class:`~collections.abc.Iterable`
        iterable to be converted into a matrix (consists of integers from 0
        to num_classes).
    num_classes : int or None
        total number of classes. If :any:`None` it will be infered from the
        data.
    dtype : str
        The data type expected by the input, as a string
        (`float32`, `float64`, `int32`...)

    Returns
    -------
    array-like of shape `(len(y), classes)`
        A binary matrix representation of the input. The classes axis
        is placed last.

    Example
    -------
    Consider an array of 5 labels out of a set of 3 classes {0, 1, 2}:

    >>> labels = [0,2,1,2,0]
    >>> to_categorical(labels)
    array([[1., 0., 0.],
           [0., 0., 1.],
           [0., 1., 0.],
           [0., 0., 1.],
           [1., 0., 0.]], dtype=float32)

    `to_categorical` converts this into a matrix with as many
    columns as there are classes. The number of rows
    stays the same.

    """

    y = np.array(y, dtype='int')
    input_shape = y.shape
    if input_shape and input_shape[-1] == 1 and len(input_shape) > 1:
        input_shape = tuple(input_shape[:-1])
    y = y.ravel()
    if not num_classes:
        num_classes = np.max(y) + 1
    n = y.shape[0]
    categorical = np.zeros((n, num_classes), dtype=dtype)
    categorical[np.arange(n), y] = 1
    output_shape = input_shape + (num_classes,)
    categorical = np.reshape(categorical, output_shape)
    return categorical

def tqdm_controlled(it, verbose=0, **kwargs):
    """Wraps the iterator with an optional printed progress bar using tqdm.

    Parameters
    ----------
    verbose : int, {0,1}
        Whether or not to output the progress bar. If 0 this function returns
        the iterator without modification.

    Returns
    -------
    iterator
        If ``verbose == 0`` this is just the given *it*, else it wraps *it* and
        will output the progress bar as the iterator is traversed.

    """
    if bool(verbose): return tqdm(it, **kwargs)
    return it

def random_dataset(samples = 10, features = 10, classes = 5):
    """Returns `(x,y)` each of length `samples`. The data has the given number of
    `features` and `classes`. The `y` data is in categorical representation.

    Parameters
    ----------
    features : int, or tuple
        Number of features (or shape) for the x data.
    classes : int
        Number of classes for the y data

    Returns
    -------
    tuple
        Generated dataset with:

        ``x.shape == (samples, features)`` and
        ``y.shape == (samples, classes)``

        or if features is a tuple

        ``x.shape == (samples, *features)`` and
        ``y.shape == (samples, classes)``

    """
    if isinstance(features, int): features = [features]
    x = np.random.sample(size = (samples, *features))
    y = np.random.sample(size = (samples, classes))
    return x,y
