{{ name }}
{{ underline }}

.. automodule:: {{ fullname }}

.. autosummary::
   :toctree: ../_autosummary
   {% for M in [functions,classes,exceptions] %}
   {% for m in  M%}
   {{fullname}}.{{ m }}
   {% endfor %}
   {% endfor %}
