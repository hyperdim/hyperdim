"""Test the docstring examples to ensure they run as written"""

import unittest
from hyperdim.hdmodel import HDModel

class HDModelInstanciationTestCase(unittest.TestCase):
    def test_features_parameter(self):
        from hyperdim.utils import random_dataset
        features_tests = [5, (3,3)]
        classes = 5
        dims = 100
        samples = 100
        for features in features_tests:
            x, y = random_dataset(features = features, classes=classes, samples=samples)

            # Try encode = True
            model = HDModel(features, classes, d=dims)
            model.fit(x,y, verbose = False)
            ypredicted = model.predict(x, verbose = False)
            self.assertTrue(ypredicted.shape == (samples, classes))

            # Try encode = False
            model = HDModel(features, classes, d=dims)
            correct_encoded_shape = [dims]
            x, _ = random_dataset(features = correct_encoded_shape, classes=classes, samples=samples)

            model.fit(x,y, encode=False, verbose = False)
            ypredicted = model.predict(x, encode=False, verbose = False)
            self.assertTrue(ypredicted.shape == (samples, classes))

    def test_encoder_parameter(self):
        from hyperdim.utils import random_dataset
        from collections import namedtuple
        classes = 5
        dims = 100
        samples = 10

        Wrapping = namedtuple('Encoder', ['encode'])
        enc = Wrapping(encode = lambda x:x)

        # Encoder can be str ID, callable or have the encode method
        encoders = [("nonlinear",dims,{"verbose":0}), (lambda x: x, (dims,), dict()), (enc,(dims,), dict())]
        for encoder, features_shape, encoder_args in encoders:

            x, y = random_dataset(features = features_shape, classes=classes, samples=samples)
            model = HDModel(features_shape, classes, d=dims, encoder=encoder, encoder_args = encoder_args)

            model.fit(x,y, verbose = False)

            ypredicted = model.predict(x, verbose = False)
            self.assertTrue(ypredicted.shape == (samples, classes))
