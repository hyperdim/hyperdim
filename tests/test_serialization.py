"""Test the serialization module"""

import unittest
from collections import namedtuple
from numpy.random import random_sample
from numpy import array, array_equal
import numpy as np
from hyperdim.encoders import EncoderNonLinear
from hyperdim.utils import random_dataset
from hyperdim.serialization import encode_to_file, generate_from_file
from os import remove

class EncodeGenerateTestCase(unittest.TestCase):
    def test_float32_write_open(self):
        # Mock encoder
        features = 10
        samples = 10
        dtype = 'float32'

        e = namedtuple("Encoder","encode")(lambda x: x)
        x, y = random_dataset(samples=samples,features=features)
        x = np.array(x,dtype=dtype)
        y = np.array(y,dtype=dtype)
        d1 = np.array(list(zip(x,y)))

        encode_to_file(lambda x:x, d1, e, "test.hdim",x_dtype=dtype,y_dtype=dtype)
        d2 = generate_from_file("test.hdim",x_dtype=dtype,y_dtype=dtype)
        self.assertTrue(len(d2) == samples)
        for (x1,y1),(x2,y2) in zip(d1,d2):
            x1 = e.encode([x1])[0]
            self.assertTrue(array_equal(x1,x2))
            self.assertTrue(array_equal(y1,y2))
        remove("test.hdim")

    def test_notype_write_open(self):
        # Mock encoder
        features = 10
        samples = 10

        e = EncoderNonLinear(d=1000,features=features)
        x, y = random_dataset(samples=samples,features=features)
        x = np.array(x)
        y = np.array(y)
        d1 = np.array(list(zip(x,y)))

        dtype = x.dtype

        encode_to_file(lambda x:x, d1, e, "test.hdim",x_dtype=dtype,y_dtype=dtype)
        d2 = generate_from_file("test.hdim",x_dtype=dtype,y_dtype=dtype)
        self.assertTrue(len(d2) == samples)
        for (x1,y1),(x2,y2) in zip(d1,d2):
            x1 = e.encode([x1])[0]
            self.assertTrue(array_equal(x1,x2))
            self.assertTrue(array_equal(y1,y2))
        remove("test.hdim")
